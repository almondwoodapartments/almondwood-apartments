Looking for the right mix of convenience and amenities in your UC Davis off-campus student housing? You can find it here at Almondwood Apartments. Call +1(530) 753-2115 for more info!

Address: 1212 Alvarado Ave, Davis, CA 95616, USA

Phone: 530-753-2115

Website: https://www.davisapartmentsforrent.com/almondwood